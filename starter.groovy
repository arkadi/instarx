@Grapes([
    @Grab('io.reactivex:rxgroovy:1.0.0'),
    @Grab('org.codehaus.gpars:gpars:1.2.1'),
    @Grab('com.github.groovy-wslite:groovy-wslite:1.1.0')])

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import rx.Observable
import rx.Subscriber
import rx.schedulers.Schedulers
//import java.util.concurrent.ForkJoinPool
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import jsr166y.ForkJoinPool
import static groovyx.gpars.GParsPool.executeAsync
import static groovyx.gpars.GParsPool.withExistingPool
import static groovyx.gpars.GParsPool.withPool
import wslite.rest.RESTClient

String participant = 'change-me'
String clientId = 'fa5fac604b764ac5aaab05ea74e8cae3' // obtain your own at https://instagram.com/developer
String api = 'https://api.instagram.com/v1'
//String ui = 'http://rxdisplay.neueda.lv/in'
String ui = 'http://localhost:5050/ui'
int interval = 5 // seconds
int count = 10 // count of tagged media to return
def tags = 'invent your own'.split(' ')
int limit = 3 // number of tags per observable stream

// https://github.com/jwagenleitner/groovy-wslite#rest
def instaRest = new RESTClient(api)
def uiRest = new RESTClient(ui)
def fjp = new ForkJoinPool(limit)
def scheduler = Schedulers.newThread()

@EqualsAndHashCode @ToString(includeNames=true)
class Location { Double latitude; Double longitude; Integer id; String name; }
@EqualsAndHashCode(includes = 'url') @ToString
class Media { String tag; String url; Location location; String participant; }

def unjson(json) {
    json.data.collect { post ->
        def l = post.location
        def location = l ? new Location(latitude: l.latitude, longitude: l.longitude, id: l.id, name: l.name) : null
        new Media(url: post.images.thumbnail.url, location: location)
    }
}

def media = { String tag ->
    def json = instaRest.get(path: "/tags/$tag/media/recent", query: [ client_id: clientId, count: count ]).json
    def media = unjson(json)
    media.each { it.tag = tag }
    media
}

println 'Starting...'

// step one: single-threaded HTTP fetch
def ticker = Observable.interval(interval, TimeUnit.SECONDS)
def obstags = Observable.from(tags.take(limit))
def nested /*Observable<Observable<List<Media>>>*/ = ticker.map { _seq ->
    /*Observable<List<Media>>*/ obstags.flatMap { tag ->
        try {
            Observable.just(media(tag))
        } catch (e) {
            e.printStackTrace()
            Observable.empty()
        }
    }
}
def list /*Observable<List<Media>>*/ = Observable.merge(nested)

list.subscribe { images -> println(images) }

// step two: flatten
def instagram /*Observable<Media>*/ = Observable.create { Subscriber<Media> observer ->
    list.subscribe(
        { images -> if (!observer.unsubscribed) images.each { image -> observer.onNext(image) } },
        { ex -> observer.onError(ex) },
        { -> observer.onCompleted() }
    )
}

// step three: send to UI
instagram.subscribe { image ->
    image.participant = participant
    uiRest.post() {
        type 'application/json'
        charset 'UTF-8'
        text new JsonBuilder(image).toString()
    }
}

// step four: deduplicate
long start = System.currentTimeMillis()
def limited = instagram.takeWhile { _ -> (System.currentTimeMillis() - start) < 20000 } .replay()
def threaded = limited.observeOn(scheduler)
threaded.subscribe(
    { image -> println(image) },
    { ex -> println('===== error: ' + ex) },
    { -> println('===== completed'); Thread.sleep(1000); System.exit(0) }
)
threaded.count().forEach { sz -> println("===== unfiltered stream size is $sz") }
threaded.distinct().count().forEach { sz -> println("===== deduplicated stream size is $sz") }
limited.connect()


Thread.sleep(100000)
