package instarx

import scala.collection.JavaConversions._
import scala.util.control.Breaks._
import scala.concurrent.ExecutionContext.global
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.json4s.jackson.Serialization
import org.json4s.jackson.Serialization.{read, write}
import rx.lang.scala.{Observable, Observer, Subscriber, Subscription}
import rx.lang.scala.subjects.ReplaySubject
import rx.lang.scala.schedulers.NewThreadScheduler
import dispatch._, Defaults._

object Starter {

  val log = org.slf4j.LoggerFactory.getLogger(getClass)

  val participant = Some("change-me")
  val clientId = "fa5fac604b764ac5aaab05ea74e8cae3" // obtain your own at https://instagram.com/developer
  val api = "https://api.instagram.com/v1"
  //val ui = "http://rxdisplay.neueda.lv/in"
  val ui = "http://localhost:5050/ui"
  val interval = 5.seconds
  val count = 10 // count of tagged media to return
  val tags = "invent your own".split(" ").toSeq
  val limit = 3 // number of tags per observable stream
  val scheduler = NewThreadScheduler()

  case class Location(latitude: Option[Double], longitude: Option[Double], id: Option[BigInt], name: Option[String])
  case class Media(tag: String, url: String, location: Option[Location], participant: Option[String] = None)

  // Parse Instagram media response to extract image URL and location (if any)
  private def unjson(text: String)(implicit tag: String): Seq[Media] = {
    implicit val formats = DefaultFormats
    val json = parse(text)
    val media/*: Seq[Media]*/ = for {
      JArray(posts) <- json \ "data"
      post <- posts
      JString(url) <- post \ "images" \ "thumbnail" \ "url"
    } yield Media(tag, url, (post \ "location").toOption.map(_.extract[Location]))
    media
  }

  def main(args: Array[String]) {
    // step one: multi-threaded HTTP fetch
    val ticker = Observable.interval(interval)
    val obstags = Observable.from(tags.take(limit))
    val nested/*: Observable[Observable[Seq[Media]]]*/ = ticker.map { _ =>
      obstags.flatMap { implicit tag =>
        val req = url(s"$api/tags/$tag/media/recent?client_id=$clientId&count=$count")
        val future/*: Future[Seq[Media]]*/ = Http(req OK as.String).map { resp =>
          unjson(resp)
        } .recover { case e: Exception => Nil }
        Observable.from(future)
      }
    }
    val list/*: Observable[Seq[Media]]*/ = nested.flatten

    list.subscribe(images => println(images))

    // step two: flatten
    val instagram = Observable((subscriber: Subscriber[Media]) => {
      list.subscribe(
        images => if (!subscriber.isUnsubscribed) images.foreach(subscriber.onNext),
        ex => subscriber.onError(ex),
        () => subscriber.onCompleted())
    })

    // step three: send to UI
    instagram.subscribe { image =>
      implicit val formats = Serialization.formats(NoTypeHints)
      val req = url(ui).setContentType("application/json", "UTF-8").setBody(write(image.copy(participant = participant)))
      Await.ready(Http(req.POST), 2.seconds)
    }

    // step four: deduplicate
    val start = System.currentTimeMillis
    val limited = instagram.takeWhile(_ => (System.currentTimeMillis - start) < 20000).replay
    val threaded = limited.observeOn(scheduler) // introduce some concurrency
    threaded.subscribe(images => println(images), ex => println("===== error: " + ex), () => { println("===== completed"); Thread.sleep(1000); System.exit(0) })
    threaded.size.foreach(sz => println(s"===== unfiltered stream size is $sz"))
    threaded.distinct(_.url).size.foreach(sz => println(s"===== deduplicated stream size is $sz"))
    limited.connect


    Thread.sleep(10000)
  }
}
