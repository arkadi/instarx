package instarx

import scala.collection.JavaConversions._
import scala.util.control.Breaks._
import scala.concurrent.ExecutionContext.global
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.json4s.jackson.Serialization
import org.json4s.jackson.Serialization.{read, write}
import rx.lang.scala.{Observable, Observer, Subscriber, Subscription}
import rx.lang.scala.subjects.ReplaySubject
import rx.lang.scala.schedulers.NewThreadScheduler
import dispatch._, Defaults._

object Main {

  val log = org.slf4j.LoggerFactory.getLogger(getClass)

  val participant = Some("change-me")
  val clientId = "fa5fac604b764ac5aaab05ea74e8cae3" // obtain your own at https://instagram.com/developer
  val api = "https://api.instagram.com/v1"
  //val ui = "http://rxdisplay.neueda.lv/in"
  val ui = "http://localhost:5050/ui"
  val interval = 5.seconds
  val count = 10 // count of tagged media to return
  val tags = "love instagood me tbt cute follow followme photooftheday happy tagforlikes beautiful girl like selfie picoftheday summer fun smile friends like4like instadaily fashion igers instalike food".split(" ").toSeq
  val limit = 3 // number of tags per observable stream
  val scheduler = NewThreadScheduler()

  case class Location(latitude: Option[Double], longitude: Option[Double], id: Option[BigInt], name: Option[String])
  case class Media(tag: String, url: String, location: Option[Location], participant: Option[String] = None)

  // Parse Instagram media response to extract image URL and location (if any)
  private def unjson(text: String)(implicit tag: String): Seq[Media] = {
    implicit val formats = DefaultFormats
    val json = parse(text)
    val media/*: Seq[Media]*/ = for {
      JArray(posts) <- json \ "data"
      post <- posts
      JString(url) <- post \ "images" \ "thumbnail" \ "url"
    } yield Media(tag, url, (post \ "location").toOption.map(_.extract[Location]))
    media
  }

  /* The life of the stream starts as a framework provided stream of timer events created
  by Observable.interval(). For every Instagram Tag a HTTP call is made to gather the list
  of Media URL-s. The Future returned by HTTP client is converted by Observable.from() into
  Observable. Then Observables are flatten()-ed to un-nest them into Observable[Media].
  */
  def main(args: Array[String]) {
    // Instagram limit is 5000 API requests per hour per client id or access token

    val instagram/*: Observable[Seq[Media]]*/ = Observable.interval(interval).map { _ =>
      Observable.from(tags.take(limit)).flatMap { implicit tag =>
        val req = url(s"$api/tags/$tag/media/recent?client_id=$clientId&count=$count")
        val future/*: Future[Seq[Media]]*/ = Http(req OK as.String).map { resp =>
          unjson(resp)
        } .recover { case e: Exception => Nil }
        Observable.from(future)
      }
    } /*Observable[Observable[Seq[Media]]]*/ .flatten /*=> Observable[Seq[Media]]*/

    //instagram.subscribe(images => println(images))

    // How to get Observable[Media]]?
    // Manual solution using (Replay)Subject which is both a Subscription and Observable:
    // the events are bridged from `instagram: Observable[Seq[Media]]` - for each Media in
    // the received sequence Subject.onNext() is called.
    val instagram2 = Observable((subscriber: Subscriber[Media]) => {
      val rsubj = ReplaySubject[Media]
      instagram.subscribe(images => images.foreach(rsubj.onNext), ex => rsubj.onError(ex), () => rsubj.onCompleted())
      rsubj.subscribe(subscriber)
    })
    val instagram3 = Observable.create((observer: Observer[Media]) => {
      val rsubj = ReplaySubject[Media]
      instagram.subscribe(images => images.foreach(rsubj.onNext), ex => rsubj.onError(ex), () => rsubj.onCompleted())
      rsubj.subscribe(observer)
    })

    // What if there is no Observable to subscribe to? How to build an Observable from scratch?
    // https://github.com/ReactiveX/RxScala/blob/0.x/examples/src/main/scala/SyncObservable.scala
    // Here instagram.subscribe() - a subscription to Observable is used, but it could be anything
    // else: Future.onComplete(), Actor body, an imperative piece of code that decides the event
    // must be pushed to subscriber, etc.
    // Follow the protocol:
    // (1) check for Subscriber.isUnsubscribed, (2) call onNext to push the data, (3) finish the
    // stream with onCompleted, (4) signal the error _and_ finish the stream with onError.
    val instagram4 = Observable((subscriber: Subscriber[Media]) => {
      var s: Subscription = null
      s = instagram.subscribe(
        images => if (!subscriber.isUnsubscribed) images.foreach(subscriber.onNext) else if (s != null) s.unsubscribe(),
        ex => subscriber.onError(ex),
        () => subscriber.onCompleted())
    })

    // A simpler solution by using nested Observables
    val instagram5/*: Observable[Media]*/ = Observable.interval(interval).map { _ =>
      Observable.from(tags.drop(limit).take(limit)).flatMap { implicit tag =>
        val req = url(s"$api/tags/$tag/media/recent?client_id=$clientId&count=$count")
        val future/*: Future[Observable[Media]]*/ = Http(req OK as.String).map { resp =>
          Observable.from(unjson(resp)) // notice the difference: Seq[Media] => Observable[Media]
        } .recover { case e: Exception => Observable.empty }
        Observable.from(future)
      }
    } /*Observable[Observable[Observable[Media]]]*/ .flatten.flatten /*=> Observable[Media]*/


    // Instagram's media/recent query may return images already pulled in previous run.
    // Apply Observable.distinct() to filter out duplicates.
    val start = System.currentTimeMillis
    val limited = instagram5.takeWhile(_ => (System.currentTimeMillis - start) < 20000).replay
    val threaded = limited.observeOn(scheduler) // introduce some concurrency
    threaded.subscribe(images => println(images), ex => println("===== error: " + ex), () => { println("===== completed"); Thread.sleep(1000); System.exit(0) })
    threaded.size.foreach(sz => println(s"===== unfiltered stream size is $sz"))
    threaded.distinct(_.url).size.foreach(sz => println(s"===== deduplicated stream size is $sz"))
    limited.connect

    // smooth the delivery of images (advanced task) - create Smooth (Average) operator, like
    // Sample and Debounce, but (1) no event loss and (2) internal adaptive trigger that track
    // incoming rate and smoothly adapts outgoing rate to keep it steady
    // 123........45......6790123.......4 => 1...2...3...4..5.....6.7.8.9.0.1.2.3.4


    // Send media URL and location to UI
    // {"tag":"tbt","url":"https://scontent.cdninstagram.com/....jpg","location":{"latitude":51.504976275,"longitude":-0.087847965,"id":225481160,"name":"The Shard London"},"participant":"change-me"}
    threaded.subscribe { image =>
      implicit val formats = Serialization.formats(NoTypeHints)
      val req = url(ui).setContentType("application/json", "UTF-8").setBody(write(image.copy(participant = participant)))
      Await.ready(Http(req.POST), 2.seconds)
    }

    Thread.sleep(10000)
  }
}
