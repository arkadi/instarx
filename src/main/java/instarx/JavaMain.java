package instarx;

import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.observables.ConnectableObservable;
import rx.schedulers.Schedulers;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONArray;
import org.json.JSONObject;

public class JavaMain {
    String participant = "change-me";
    String clientId = "fa5fac604b764ac5aaab05ea74e8cae3"; // obtain your own at https://instagram.com/developer
    String api = "https://api.instagram.com/v1";
    //String ui = "http://rxdisplay.neueda.lv/in";
    String ui = "http://localhost:5050/ui";
    int interval = 5; // seconds
    int count = 10; // count of tagged media to return
    String[] tags = "invent your own".split(" ");
    Scheduler scheduler = Schedulers.newThread();

    public static void main(String[] args) {
        new JavaMain().run();
    }

    // http://unirest.io/java.html
    List<Media> media(String tag) throws UnirestException {
        HttpResponse<JsonNode> resp = Unirest.get(String.format("%s/tags/%s/media/recent", api, tag))
            .queryString("client_id", clientId)
            .queryString("count", count)
            .asJson();
        return unjson(resp.getBody().getObject()).stream().map(image -> image.setTag(tag)).collect(Collectors.toList());
    }

    void run() {
        // step one: single-threaded HTTP fetch
        Observable<Long> ticker = Observable.interval(interval, TimeUnit.SECONDS);
        Observable<String> obstags = Observable.from(tags);
        Observable<Observable<List<Media>>> nested = ticker.map(_seq ->
            /*Observable<List<Media>>*/ obstags.flatMap(tag -> {
            try {
                return Observable.just(media(tag));
            } catch (Exception e) {
                e.printStackTrace();
                return Observable.empty();
            }
        }));
        Observable<List<Media>> list = Observable.merge(nested);

        list.subscribe(images -> images.forEach(image -> System.out.println("list: " + image)));

        // step two: flatten
        Observable<Media> instagram = Observable.create(/*Subscriber<Media>*/ observer -> {
            list.subscribe(
                    images -> {
                        if (!observer.isUnsubscribed()) images.forEach(image -> observer.onNext(image));
                    },
                    observer::onError,
                    observer::onCompleted
            );
        });

        // step three: send to UI
        instagram.subscribe(image -> {
            try {
                Unirest.post(ui).body(new JSONObject(image.setParticipant(participant)).toString()).asBinary();
            } catch (UnirestException e) {
                e.printStackTrace();
            }
        });

        // step four: deduplicate
        long start = System.currentTimeMillis();
        ConnectableObservable<Media> limited = instagram.takeWhile( _image -> (System.currentTimeMillis() - start) < 20000) .replay();
        Observable<Media> threaded = limited.observeOn(scheduler);
        threaded.subscribe(
                System.out::println,
                ex -> System.out.println("===== error: " + ex),
                () -> {
                    System.out.println("===== completed");
                    sleep(1000);
                    System.exit(0);
                }
        );
        threaded.count().forEach(sz -> System.out.println("===== unfiltered stream size is " + sz));
        threaded.distinct(image -> image.getUrl()).count().forEach(sz -> System.out.println("===== deduplicated stream size is " + sz));
        limited.connect();


        sleep(100000);
    }

    void sleep(long msec) {
        try { Thread.sleep(msec); } catch (InterruptedException e) { throw new RuntimeException(e); }
    }

    List<Media> unjson(JSONObject json) {
        JSONArray posts = json.getJSONArray("data");
        List<Media> media = new ArrayList<>(posts.length());
        for (int i = 0; i < posts.length(); ++i) {
            JSONObject post = posts.getJSONObject(i);
            JSONObject l = post.optJSONObject("location");
            Location location = l == null ? null : new Location(
                l.optDouble("latitude"), l.optDouble("longitude"), l.has("id") ? l.getInt("id") : null, l.has("name") ? l.optString("name") : null);
            media.add(new Media(post.getJSONObject("images").getJSONObject("thumbnail").getString("url"), location));
        }
        return media;
    }

    public class Location {
        Double latitude; Double longitude; Integer id; String name;
        public Location(Double latitude, Double longitude, Integer id, String name) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.id = id;
            this.name = name;
        }
        @Override
        public String toString() {
            return "Location{" +
                    "latitude=" + latitude +
                    ", longitude=" + longitude +
                    ", id=" + id +
                    ", name='" + name + '\'' +
                    '}';
        }
        public Double getLatitude() {
            return latitude;
        }
        public Double getLongitude() {
            return longitude;
        }
        public Integer getId() {
            return id;
        }
        public String getName() {
            return name;
        }
    }

    public class Media {
        String tag; String url; Location location; String participant;
        public Media(String url, Location location) {
            this.url = url;
            this.location = location;
        }
        public Media setTag(String tag) {
            this.tag = tag;
            return this;
        }
        public Media setParticipant(String participant) {
            this.participant = participant;
            return this;
        }
        @Override
        public String toString() {
            return "Media{" +
                    "tag='" + tag + '\'' +
                    ", url='" + url + '\'' +
                    ", location=" + location +
                    ", participant='" + participant + '\'' +
                    '}';
        }
        public String getTag() {
            return tag;
        }
        public String getUrl() {
            return url;
        }
        public Location getLocation() {
            return location;
        }
        public String getParticipant() {
            return participant;
        }
    }
}
