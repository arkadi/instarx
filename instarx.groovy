@Grapes([
    @Grab('io.reactivex:rxgroovy:1.0.0'),
    @Grab('org.codehaus.gpars:gpars:1.2.1'),
    @Grab('com.github.groovy-wslite:groovy-wslite:1.1.0')])

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import rx.Observable
import rx.Subscriber
import rx.schedulers.Schedulers
//import java.util.concurrent.ForkJoinPool
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import jsr166y.ForkJoinPool
import static groovyx.gpars.GParsPool.executeAsync
import static groovyx.gpars.GParsPool.withExistingPool
import static groovyx.gpars.GParsPool.withPool
import wslite.rest.RESTClient

String participant = "change-me"
String clientId = "fa5fac604b764ac5aaab05ea74e8cae3" // obtain your own at https://instagram.com/developer
String api = "https://api.instagram.com/v1"
//String ui = "http://rxdisplay.neueda.lv/in"
String ui = "http://localhost:5050/ui"
int interval = 5 // seconds
int count = 10 // count of tagged media to return
def tags = "love instagood me tbt cute follow followme photooftheday happy tagforlikes beautiful girl like selfie picoftheday summer fun smile friends like4like instadaily fashion igers instalike food".split(" ")
int limit = 3 // number of tags per observable stream

def instaRest = new RESTClient(api)
def uiRest = new RESTClient(ui)
def fjp = new ForkJoinPool(limit)
def scheduler =  Schedulers.newThread()

@EqualsAndHashCode @ToString(includeNames=true)
class Location { Double latitude; Double longitude; Integer id; String name; }
@EqualsAndHashCode(includes = "url") @ToString
class Media { String tag; String url; Location location; String participant; }

def unjson(json) {
    json.data.collect { post ->
        def l = post.location
        def location = l ? new Location(latitude: l.latitude, longitude: l.longitude, id: l.id, name: l.name) : null
        new Media(url: post.images.thumbnail.url, location: location)
    }
}

def media = { String tag ->
    def json = instaRest.get(path: "/tags/$tag/media/recent", query: [ client_id: clientId, count: count ]).json
    def media = unjson(json)
    media.each { it.tag = tag }
    media
}

println 'Starting...'

// single-threaded HTTP fetch
                                            // Observable<Observable<List<Media>>> => Observable<List<Media>>
def instagram /*Observable<List<Media>>*/ = Observable.merge(Observable.interval(interval, TimeUnit.SECONDS).map { _seq ->
    Observable.from(tags.take(limit)).flatMap { tag -> // Observable<List<Media>>
        try {
            Observable.just(media(tag)) // .from()
        } catch (Exception e) {
            e.printStackTrace()
            Observable.empty()
        }
    }
})

// multi-threaded HTTP fetch
def instagram2 /*Observable<List<Media>>*/ = Observable.interval(interval, TimeUnit.SECONDS).flatMap { _seq ->
    Observable.merge(withExistingPool(fjp) { _pool -> // List<Observable<List<Media>>> => Observable<List<Media>>
        executeAsync( /*List<Closure<List<Media>>>*/ tags.take(limit).collect { tag -> { ->
            try {
                media(tag)
            } catch (Exception e) {
                e.printStackTrace()
                []
            }
        }}).collect { future -> Observable.from(future) } // List<Future<List<Media>>> => List<Observable<List<Media>>>
    })
}

// flatten
def instagram3 = Observable.create { Subscriber<Media> observer ->
    instagram2.subscribe(
        { images -> images.each { image -> observer.onNext(image) } },
        { ex -> observer.onError(ex) },
        { -> observer.onCompleted() }
    )
}

//instagram3.subscribe { images -> println(images) }

// de-duplicate
long start = System.currentTimeMillis()
def limited = instagram3.takeWhile { _ -> (System.currentTimeMillis() - start) < 20000 } .replay()
def threaded = limited.observeOn(scheduler)
threaded.subscribe(
    { image -> println(image) },
    { ex -> println("===== error: " + ex) },
    { -> println("===== completed"); Thread.sleep(1000); System.exit(0) }
)
threaded.count().subscribe { sz -> println("===== unfiltered stream size is $sz") }
threaded.distinct().count().subscribe { sz -> println("===== deduplicated stream size is $sz") }
limited.connect()

// send to UI
threaded.subscribe { image ->
    image.participant = participant
    uiRest.post() {
        type "application/json"
        charset "UTF-8"
        text new JsonBuilder(image).toString()
    }
}

Thread.sleep(100000)
